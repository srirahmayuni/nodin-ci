<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>COBAIN YUKS!</title>
      <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url("application/views/plugins/font-awesome/css/font-awesome.min.css")?>">
  <link rel="stylesheet"  href="<?php echo base_url("application/views/plugins/font-awesome/css/font-awesome.min.css")?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url("application/views/dist/css/adminlte.min.css")?>">
  <!-- iCheck -->
  <link rel="stylesheet"  href="<?php echo base_url("application/views/plugins/iCheck/flat/blue.css")?>">
  <!-- Morris chart -->
  <link rel="stylesheet"  href="<?php echo base_url("application/views/plugins/morris/morris.css")?>">
  <!-- jvectormap -->
  <link rel="stylesheet"  href="<?php echo base_url("application/views/plugins/jvectormap/jquery-jvectormap-1.2.2.css")?>">
  <!-- Date Picker -->
  <link rel="stylesheet"  href="<?php echo base_url("application/views/plugins/datepicker/datepicker3.css")?>">
  <!-- Daterange picker -->
  <link rel="stylesheet"  href="<?php echo base_url("application/views/plugins/daterangepicker/daterangepicker-bs3.css")?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet"  href="<?php echo base_url("application/views/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css")?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">

       <div class="card">
              <div class="card-header border-transparent">
                <h3 class="card-title">Nodin Integrasi</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove">
                    <i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="scroll">
              <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Action</th>
                      <th>Sow Id</th>
                      <th>Program ID</th>
                      <th>Site ID</th>
                      <th>Program</th>
                      <th>Band</th>
                      <th>Process</th>
                      <th>Process Sow</th>
                      <th>Vendor EQP</th>
                      <th>Contract Number EQP</th>
                      <th>Tsow ID</th>
                      <th>JPP Year</th>
                      <th>Site Name</th>
                      <th>Longitude</th>
                      <th>Latitude</th>
                      <th>EQP Type</th>
                      <th>Region</th>
                      <th>Program Capex</th>
                      <th>Infra Status</th>
                      <th>PS Status</th>
                      <th>TRM Vendor</th>
                      <th>TRM Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>
                          <input type="checkbox" value="" name="">
                                                <td>421135549</td>
                      <td>ADL013NEW ELEMENTCOLLOCATED 4GLTE18002018</td>
                      <td>ADL013</td>
                      <td>Collocated 4G</td>
                      <td>LTE1800</td>
                      <td>EQP</td>
                      <td>Collocated 4G</td>
                      <td>ZTE</td>
                      <td>4384</td>
                      <td>ADL013NEW ELEMENTCOLLOCATED 4GLTE1800EQPCOLLOCATED 4GZTE4384</td>
                      <td>2018</td>
                      <td>Poleang Timur</td>
                      <td>121.80501</td>
                      <td>-4.83278</td>
                      <td>TYPE-2M</td>
                      <td>R9 Sulawesi</td>
                      <td>New Element</td>
                      <td>RFI</td>
                      <td>Null</td>
                      <td>Null</td>
                      <td>Null</td>
                      </td>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Submit Nodin</a>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>

    <!-- /.content -->
    <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
<script src="<?php echo base_url(); ?>plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url(); ?>plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(); ?>plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url(); ?>plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url(); ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url(); ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(); ?>dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>dist/js/demo.js"></script>></script>
</body>
</html>
