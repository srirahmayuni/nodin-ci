<?php
	class Siswa extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model('SiswaModel');
		}
		public function index()
		{
			$data['siswa']=$this->SiswaModel->view();
			$this->load->view('siswa/index',$data);
		}
	}