<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class DafinciController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    function index(){  
        $this->load->view('nodin_view');
    }


    //Menampilkan data dafinci-po
    function index_get() {
        $swoid = $this->get('swoid');
        if ($swoid == '') {
            $this->db->where('infra_status','RFI');
            $kontak = $this->db->get('dafinci_po')->result();
        } else {
            $this->db->where('swoid', $id);
            $kontak = $this->db->get('dafinci_po')->result();
        }
        $this->response($kontak, 200);
    }

    //Menambahkan data dafinci-po
    function postDafinci(){
        $data = array(
            'swoid' => $this->post('swoid'),
            'program_id' => $this->post('program_id'),
            'siteid' => $this->post('siteid'),
            'program' => $this->post('program'),
            'band' => $this->post('band'),
            'process' => $this->post('process'),
            'process_sow' => $this->post('process_sow'),
            'vendor_eqp' => $this->post('vendor_eqp'),
            'contract_num_eqp' => $this->post('contract_num_eqp'),
            'tsowid' => $this->post('tsowid'),
            'jpp_year' => $this->post('jpp_year'),
            'site_name' => $this->post('site_name'),
            'longitude' => $this->post('longitude'),
            'latitude' => $this->post('')
        );
    }
}
?>