<?php
	class nodin extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model('nodinmodel');
		}
		public function index()
		{
			$this->load->view('nodin_view');
		}
	}